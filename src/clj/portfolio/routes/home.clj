(ns portfolio.routes.home
  (:require
    [portfolio.layout :as layout]
    [clojure.java.io :as io]
    [portfolio.middleware :as middleware]
    [ring.util.http-response :as response]))

(defn home-page [request]
  (layout/render request "home.html" {:docs (-> "docs/home.md" io/resource slurp)}))

(defn resume-page [request]
  (layout/render request "home.html" {:docs (-> "docs/resume.md" io/resource slurp)}))

(defn blog-page [request]
  (layout/render request "home.html" {:docs (-> "docs/blog.md" io/resource slurp)}))

(defn contact-page [request]
  (layout/render request "home.html" {:docs (-> "docs/contact.md" io/resource slurp)}))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/resume" {:get resume-page}]
   ["/blog" {:get blog-page}]
   ["/contact" {:get contact-page}]])

