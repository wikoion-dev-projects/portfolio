# portfolio
`KISS` - simple html/css only portfolio application written in clojure

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:
```
lein run
```

Can also be built as a docker container:
```
cd portfolio
docker build -t portfolio .
docker run -d -p 3000:3000 portfolio
```
