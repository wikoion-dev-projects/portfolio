# Richard Draycott  

### Infrastructure & Devops Specialist
#### Skills
- Java, Kotlin, Clojure, Ruby and other languages  
- Spring, Rails, Luminus and other frameworks
- Docker, Kubernetes development and deployments  
- Bash, Powershell, Ansible, Puppet and more
- AWS, Azure and Digital Ocean cloud platforms
- KVM, ESXI, Hyper-V and Xen
- Networking infrastructure
- Agile Practices

#### Highlighted Projects
- [Portfolio](https://gitlab.com/wikoion-dev-projects/portfolio) - This website; a simple, lightweight, dockerised clojure webapp, without any Javascript, or even Clojurescript for that matter!  
- [Blog](https://gitlab.com/wikoion-dev-projects/draycott-live-blog) - A blogging platform, written using Java, Kotlin and Spring. The project enables users to register, create new posts, allow third party sign ups, communicate in a forum and more - *Work in Progress*  
- [Self Hosted](https://gitlab.com/self-hosted-container-services) - Docker stacks providing example configuration for various containerised services
- [Dotfiles](https://gitlab.com/system-configuration-files/dotfiles) - Configuration files for Nord themed i3WM, tmux, vim and polybar

#### Quick Links
- [Gitlab](https://gitlab.com/wikoion)
- [Github](https://github.com/wikoion)
- [Twitter](https://twitter.com/callmejonnson)
