# Richard Draycott - Curriculum Vitae

### Contact
- Email: richard@draycott.live
- [Gitlab](https://gitlab.com/wikoion)
- [Github](https://github.com/wikoion)

### Statement
Senior Developer and DevOps Engineer with extensive experience designing and building a variety of applications written in various languages including: Python, Golang and Rust. In my last roles I have worked on large open source code bases as a senior developer, built kubernetes infrastructure on AWS, GCP and Azure and operationally supported the applications running on them. I've worked on multiple kubernetes controllers, including writing new controllers from scatch, with and without kubebuilder. I have experience writing terraform providers, modules and deploying complex infrastructure with terraform. I also have extensive experience provisioning infrastructure using clusterapi and ack. In my last role there was a strong focus on the developers who write the software being able to operationally support it, so I have experience building and delivering soltuions end to end, including CI/CD and monitoring/alerting.

### Key Skills
- Ability to write code and develop software in various languages including: Java, Kotlin, Ruby, Clojure, Go, Python and Rust.
- Building and managing kubernetes cluster and writing k8s controllers with kubebuilder.
- Terraform, building modules and writing providers.
- GCP, Azure and AWS cloud and hybrid deployments including: EKS, ECS, Lambda and On-Prem Kubernetes.
- Experience with a variety of monitoring platforms including: Prometheus, Grafana, SCCM and Icinga/Nagios.
- Building CI/CD solutions using Gitlab CI, Jenkins, Circle CI and Concourse.
- Knowledge of Timeseries, SQL and NoSQL databases including InfluxDB, Postgres, MongoDB, Redis and GraphQL.
- Various provisioning tooling including Ansible, Chef and Puppet.
- Bash, Powershell and Groovy scripting.
- Implementation, use and automation of JIRA and other Atlassian products.
- Extensive knowledge of various virtualisation technologies including: VMware ESXI, Hyper-V, Xen, o-virt and KVM.
- Implementing storage and backup solutions using vendors such as: Veeam, Datto, Storage Craft and Altaro.
- SAN infrastructure configuration and deployment, experience with: Nimble/HPE, Synology and FreeNas.
- Extensive knowledge of Microsoft technologies such as: Active Directory, Exchange, WSUS, WDS etc. (On-prem and cloud).
- Linux System Administration; extensive experience with: Redhat/Centos, Ubuntu, Debian, Arch and Gentoo.
- Watchguard, Cisco, Sophos, Sonicwall and Pfsense firewall installation and configuration.
- Netgear, Cisco, Aruba and Ubiquiti networking technologies.

### Employment
#### Influxdata
**Senior Software Engineer**
**August 2022 - Present**

Whilst at Influxdata I have worked on the InfluxDB 2.0 Storage team where I was responsible for mainting a large open source golang code base and managing the operation of the multi tenant service hosted on AWS, GCP and Azure. Responsibilities included identifying and fixing bugs in the database code, implementing new features, building kubernetes controllers and operationally supporting the database/kubernetes infrastructure.
Since the release of InfluxDB 3.0 I worked on the team responsible for building the Cloud Dedicated product from the ground up. This work involved designing and building a complete soulution that, using multiple kubernetes controllers, would create entire cluster's from scratch in AWS using ack and clusterapi, the controller had to be able to provision multiple VPCs, peering connections, RDS cluster and S3 bucket, AWS Machine Images and EC2 instances. As a team we were respsonsible for wriiting the code to provision all of the clusters, building the monitoring infrastructure the CI/CD pipeline. CI/CD presented a unique challenge since new code changes to the database, that had been vetted, needed to be synced to many clusters simultaniously, to achieve this we created a system that bundles up the latest code, pushes to an artifact registry, then we use flux running inside each cluster to pull and sync the latest bundle version.

#### Sky Betting and Gaming
**Gaming Platform DevOps Engineer**
**January 2020 - August 2022**

In this role I've been working as part of an agile team that has been focused on providing a platform and technical advice for the developers around the Gaming department. I have been responsible for creating and maintaining automation and tooling to allow the game and application developers to easily deploy their code to AWS or on-premise depending on their requirements. To do this we maintain centralised Terraform modules for various AWS technologies such as: Lambda, SQS, ECS, EKS, S3 etc. We also maintain Terraform modules that wrap various helm charts, such as: Istio, InfluxDB, MariaDB, MongoDB etc. Enabling simple deployment of resources to Kubernetes. The dev teams add the Terraform modules that they need to their Bitbucket repo and can deploy their stack with the core Jenkins Pipeline we maintain.

One of my key projects in this team was to provide a central set of Istio Ingress Gateways, backed by F5 Load Balancers, for our AWS and on-premise Kubernetes clusters. I then made Terraform modules that wrapped various Istio custom resources, the dev teams could then simply add an Isito Virtual Service module to their existing Terraform to enable ingress to their applications. One of the challenges with this project is that we wanted to leverage the Cert Manager integration Istio offers to simplify the management of Vault issued certificates. The Cert Manager/Istio Gateway secret discovery will however, only work in the namespace in which your core gateway is deployed. Our solution to this was to have a central repository in Bitbucket containing the core Terraform for our Istio deployment. Teams could then make a PR to this repo to add additional Gateway and Certificate custom resources, using the central Terraform modules, with their desired host names and certificate configurations. There is then a Jenkins pipeline that will be run on raising a PR to test those names and certificates are valid, if there aren't any errors the configuration will be automatically deployed once the PR is merged.

Another key responsibility as a Platform DevOps Engineer is maintaing the centralised monitoring platform, hosted on our on-premise Kubernetes cluster we use: Prometheus, Grafana, Alert Manager and Thanos, along with in house "exporters" to provide a single pane of glass to allow the dev teams to easily monitor their applications. To allow us to monitor and alert on all of our metrics we maintain several exporters for services such as New Relic and Cloudwatch. These are written in Go, using the prometheus-client Go library and run in Kubernetes, presenting a scrape target for our Prometheus instances running in Kubernetes. 

Another of my key projects for this team was delivering an exporter for the Go performance profiling utility pprof. The exporter consumes a Kubernetes ConfigMap of endpoints and scrape intervals, that can be updated automatically via PR to the Bitbucket repo, it will then continually scrape the pprof endpoint at the desired interval and expose the results as metrics to Prometheus. This project required a significant amount of work to re-engineer various functions of the pprof package because most of the package's functions pprof report parsing functions are internal. This was a very interesting project to work on and has given me a deep understanding of performance profiling Go applications.

#### Plusnet
**Release Engineer**
**April 2019 - December 2019**

- Writing Jenkins Pipelines.
- Troubleshooting failed Jenkins builds.
- Writing Bitbucket hooks.
- Creating Dockerfiles and automating Docker builds.
- Extensive Bash, Ansible and Puppet scripting.
- Maintenance and development of in-house Jenkins plugins.
- Maintenance of Swarm Clusters.
- Investigation of new technologies for application in future projects.
- Implementation, configuration and management of Prometheus/Grafana monitoring stack.
- Performing code releases to legacy systems, troubleshooting failures and implementing hotfixes.
- Migration from legacy platforms.
- Working with dev teams to Dockerise applications and assist with build and deployment.
- Liquibase DB management.
- Developing Java and Ruby applications.
- Test automation using Selenium.

#### Kiwi IT Solutions
**Systems Consultant**
**2017 – 2019** 

The role has included: visiting a variety of client sites, performing surveys, making site recommendations, writing job specifications, planning deployments and migrations and implementing said planned projects.

Whilst working at Kiwi I have gained experience in: designing and implementing on-prem, cloud and hybrid infrastructure deployments for a variety of clients, using technologies such as: Azure Paas, Iaas, Azure Backup, Azure Site Recovery and O365, VMware and Hyper-V installations and migrations utilising Nimble StoreVirtual storage, Veeam Backup and Replication, Watchguard Firewalls, HPE Networking, Citrix and RDS including Netscalers, Docker and Ansible.

The role has also included performing site security audits and various automation tasks for a variety of clients, these have involved: writing Powershell and Bash scripts and Ansible playbooks.

#### We Solve IT
**Infrastructure Engineer**
**2014 – 2017** 

- Implementing and maintaining Citrix Xenapp, Xendesktop and NetScaler.
- Installation and management of Vmware Vsphere and ESXI Hosts.
- Veeam backup and replication software, including remote replication.
- Active directory and group policy management and configuration Installation.
- Configuration and management of Windows Server 2008, 2008r2, 2012 and 2016.
- Configuration and management of Ubuntu and SUSE servers.
- Configuration and management of SANs.
- Exchange implementation and management.
- TMG, Untangle and Sonicwall firewalls.
- Various networking hardware e.g. Draytek Routers and Netgear switches.
- Data centre host and SAN implementations.
- 3cx PBX phone system management and provisioning new extensions and devices.
- Enterprise security systems including: Vasco Digipass two factor authentication, Symantec messaging gateway, Avast Enterprise, Sophos Endpoint and Pure Message.
- Dealing with security threats e.g. Crypto Locker.
- Implementation and management of JIRA ticketing system, including custom Groovy scripts etc.
