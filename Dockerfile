FROM clojure

COPY . /portfolio

WORKDIR /portfolio

EXPOSE 3000

VOLUME /portfolio/posts

CMD ["lein", "run"]
